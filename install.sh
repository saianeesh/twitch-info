#!/usr/bin/env bash

DIR="${0%/*}/"

INSTALL_DIR="/usr/local/bin/"
PROG_NAME="twitch-info"


Install() {
    printf "%s" "Copying ${DIR}${PROG_NAME} to ${INSTALL_DIR}${PROG_NAME}..."
    cp -p "${DIR}${PROG_NAME}" "${INSTALL_DIR}"
    printf "%s\n" "Done."
}

Uninstall() {
    printf "%s" "Removing ${INSTALL_DIR}${PROG_NAME}..."
    rm "${INSTALL_DIR}${PROG_NAME}"
    printf "%s\n" "Done."
}

main() {
    if (( $(id -u) != 0 )); then
        echo "Run this script as root!"
        exit 1
    fi
    [[ "${1}" == "-r" ]] && Uninstall || Install
}

main "${@}"
