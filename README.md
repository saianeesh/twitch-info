# Twitch Info
Uses the Twitch API to provide some command-line functionality. 


# Screenshots
![TUI-mode](images/tui.png)
![normal-mode](images/normal.png)

# Usage
Normal-mode:
```
./twitch-info cmd query
```
Interactive-mode:
```
./twitch-info
```
where `cmd` is one of:
| Command | Description |
| ------- | ------------|
| `search-channel` | search for a channel (displays `SEARCH_MAX` results) |
| `search-game` | search for a category (game) and find `SEARCH_MAX` streams for the chosen game |
| `vod` | print `SEARCH_MAX` VODs available from the given user |
| `help` | print the help text |
| `status` | get the status of `SEARCH_MAX` of the given user(s) |
| `status-following` | get the status of all the user(s) `MY_USERNAME` is following |
| `play` | play the live feed (if any) of a user | 


In interactive-mode you can exit the program by running the `exit` command. The `MY_USERNAME` variable is intended to be set to your twitch username since `status-following` uses this variable to find all the necessary user(s) (all the users `MY_USERNAME` follows). To use the TUI (`whiptail` is required for the TUI to work) set the `TUI` variable in the `twitch-info` file to `true`.


Only tested with bash 5.1.16



# Twitch API Setup
Please find instructions to setup a Twitch API [here](https://dev.twitch.tv/docs/api/get-started).




# Installation
```
./install.sh
```
If `twitch-info` is changed run `install.sh` again
* Creates a copy of `twitch-info` in `/usr/local/bin/`
* `-r` to uninstall



# Dependencies
* `mpv` (as a video player)
    * `yt-dlp` (or other similar programs)
* `curl` (for API calls)
* `jq` (for json response parsing)
* `whiptail` (for TUI)




# Examples
* `twitch-info search-channel arrge`
* `twitch-info status tsoding arrge`
* `twitch-info status-following`
* `twitch-info play tsoding`
* `twitch-info help`
